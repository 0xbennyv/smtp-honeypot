import socket
import threading
import time

host = ''
port = 8888

tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

tcpsock.bind((host,port))   

class ClientThread(threading.Thread):
    
    def __init__(self,ip,port,clientsocket):
        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.csocket = clientsocket
        print ('[+] New thread started for '+ip+':'+str(port))
    
    
    def run(self):
        motd = '220 mail.port25.com (PowerMTA(TM) v4.0) ESMTP service ready \n'
        clientsock.send (motd.encode())

        while True:
            data = self.csocket.recv(2048)
            data_decode = data.decode()
            writeLog(str(ip), data_decode)
            answer = respond(data_decode)
            if 'goodbye' in answer:
                clientsock.send(answer.encode())
                clientsock.close()
                
            else:
                clientsock.send(answer.encode())
            
 
def respond(data):
    if 'EHLO' in data.upper():
        rsp = '250-' + data[5:] + '\n'
        
    elif 'FROM' in data.upper():
        rsp = '250 2.1.0 MAIL ok \n'
        
    elif 'RCPT' in data.upper():
        rsp = '250 2.1.5 ' + data[9:] + 'ok \n'
            
    elif 'QUIT' in data.upper():
        rsp = '221 2.0.0 mail.port25.com says goodbye \n'
        
    else:
        rsp = 'Invalid Command \n'
        
    return rsp 

def writeLog(ip, data):
    fopen = open('./smtp_honeypot.txt', 'a')
    fopen.write('Time = %s, IP = %s, Data = %s'%(time.ctime(), ip, data))
    fopen.close()
       

while True:
    tcpsock.listen(4)
    print ('\nStarting Honey Pot...')
    (clientsock, (ip, port)) = tcpsock.accept()
    newthread = ClientThread(ip, port, clientsock)
    newthread.start()
    
