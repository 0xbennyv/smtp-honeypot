import time
import socket
import threading

host = ''
port = 25

def main(threading):
    print ('Starting Honey Pot...')
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mySocket.bind((host,port))
    mySocket.listen(1)
    conn, addr = mySocket.accept()
    print ("Connection from: " + str(addr))
    motd = '220 mail.port25.com (PowerMTA(TM) v4.0) ESMTP service ready \n'
    conn.send (motd.encode())
    
    while True:
            data = conn.recv(1024)
            data_decode = data.decode()
            writeLog(str(addr), data_decode)
            answer = respond(data_decode)
            if 'goodbye' in answer:
                conn.send(answer.encode())
                
            else:
                conn.send(answer.encode())
            
def respond(data):
    if 'EHLO' in data.upper():
        rsp = '250-' + data[5:] + '\n'
        return rsp
    elif 'FROM' in data.upper():
        rsp = '250 2.1.0 MAIL ok \n'
        return rsp
        
    elif 'RCPT' in data.upper():
        rsp = '250 2.1.5 ' + data[9:] + 'ok \n'
        return rsp
            
    elif 'QUIT' in data.upper():
        rsp = '221 2.0.0 mail.port25.com says goodbye \n'
        return rsp
    
    else:
        rsp = 'Invalid Command \n'
        return rsp
    
            
def writeLog(ip, data):
    fopen = open('./smtp_honeypot.txt', 'a')
    fopen.write('Time: %s\nIP: %s\nData: %s\n'%(time.ctime(), ip, data))
    fopen.close()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print ('Bye!')
        exit(0)
